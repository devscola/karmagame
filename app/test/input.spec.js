import Input from '../src/components/Input'
import QuestionForm from '../src/components/QuestionForm'

describe("Tests karma-input Component", function() {
    let componentInput
    beforeEach(() => {
        componentInput = document.createElement("karma-input")
    })

    it("Can create a change event ", function() {
        document.body.appendChild(componentInput)

        const spy = jest.fn()
        document.addEventListener('updateInputText', (event) => { spy(event) })
        const event = new Event('change', { bubbles: true })
        componentInput.shadowRoot.querySelector('input').dispatchEvent(event)

        expect(spy).toHaveBeenCalled()
    })

    it("Can launch an eventKeyUp event when key is pressed", function() {
        componentInput.setAttribute('eventName', 'eventKeyUp')
        document.body.appendChild(componentInput)

        const spy = jest.fn()
        document.addEventListener('eventKeyUp', (event) => { spy(event) })
        const keyUp = new KeyboardEvent('keyup', {keyCode: 13,  bubbles: true })
        componentInput.shadowRoot.querySelector('input').dispatchEvent(keyUp)

        expect(spy).toHaveBeenCalled()
    })
})
