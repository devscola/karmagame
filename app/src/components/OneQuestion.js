import oneQuestionTemplate from '../templates/OneQuestionTemplate.html' 
const questionTemplate = document.createElement('template')
questionTemplate.innerHTML = oneQuestionTemplate

export default class OneQuestion extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(questionTemplate.content.cloneNode(true))
        this.questionTitle = this.shadowDOM.querySelector('h1')
    }

    connectedCallback() {
        this.questionTitle.addEventListener("click", this.displayAnswers.bind(this))
        this.shadowDOM.addEventListener('closeQuestion', this.closeQuestion.bind(this))
        document.addEventListener('sendAnswer', this.addAnswerSection.bind(this),{once: true})
    }

    addAnswerSection(event) {
        const answersList = this.shadowDOM.querySelector("answers-list")
        const answers = answersList.shadowDOM.querySelectorAll("one-answer")
        const questionAndAnswersArea = this.shadowDOM.querySelector(".questionAndAnswers")
        if(answers.length > 0) {
            const answersSection = document.createElement("h5")
            answersSection.innerHTML = "Lista de respuestas"
            questionAndAnswersArea.insertBefore(answersSection, answersList)
        }
    }

    closeQuestion() {
        this.removeCloseQuestionButton()
        this.sendCloseQuestionEvent()
    }

    removeCloseQuestionButton() {
        this.shadowDOM.querySelector('karma-button').remove()
    }

    sendCloseQuestionEvent() {
        this.dispatchEvent(
            new CustomEvent('closeQuestionButton', {
                bubbles: true,
                composed: true
            })
        )
    }

    displayAnswers() {
        const answersList = this.shadowDOM.querySelector("answers-list")
        if (!answersList.hasAttribute("hidden")){
            answersList.hidden = true
        } else {
            answersList.hidden = false
        }
    }

    static get observedAttributes() {
        return ['state']
    }
}
window.customElements.define("one-question", OneQuestion)
