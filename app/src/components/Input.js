const formTemplate = document.createElement('template')
formTemplate.innerHTML = `
    <style>
       input{
        width: 30%;
        border: 2px solid #197B4C;
        border-radius: 4px;
        background-color: #F9F0E2;
        padding:5px;
        font-size:1.3em;
        color: #2A7886;
       }
       
    </style>
    <input type="text" placeholder="Escribe aquí">
`
const ENTER_KEY = 13

export default class Input extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open'})
        this.shadowDOM.appendChild(formTemplate.content.cloneNode(true))
        this.input = this.shadowDOM.querySelector('input')
        this.eventTextChange = "updateInputText"
    }

    connectedCallback() {
        this.input.addEventListener('change', this.cleanAndLaunchEvent.bind(this))

        this.input.addEventListener('keyup', (event) => {
            if (event.keyCode === ENTER_KEY) {
                this.launchEvent(this.eventKeyUp, event)
                this.cleanInput()
            }
        })
    }

    static get observedAttributes() {
        return ['name', 'eventName']
    }

    get eventKeyUp() {
        return this.getAttribute('eventName')
    }

    cleanAndLaunchEvent(event) {
        this.launchEvent(this.eventTextChange,event)
        this.cleanInput()
    }

    cleanInput() {
        const input = this.shadowDOM.querySelector('input')
        input.value = ''
    }
    
    launchEvent(eventName,event) {
        this.dispatchEvent(
            new CustomEvent(eventName, {
                bubbles: true,
                detail: event.target.value
            })
        )
    }   
}

window.customElements.define("karma-input", Input)