const questionList = document.createElement('template')
questionList.innerHTML = `
    
`
export default class QuestionList extends HTMLElement {

    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(questionList.content.cloneNode(true))
        this.url = "http://localhost:3000"
        this.loadClosedQuestions()
        this.loadActiveQuestion()
    }

    connectedCallback() {
        document.body.addEventListener("closeQuestionButton", this.changeQuestionStateToClose.bind(this))
        document.body.addEventListener('sendQuestion', this.appendQuestion.bind(this))
        document.body.addEventListener("sendAnswer", this.addAnswersToQuestion.bind(this))
    }

    loadClosedQuestions() {
        this.getClosedQuestions()
            .then(questions => questions.forEach(question => {
                console.log(question);
                const answers = question.answers.answers
                const questionComponent = this.createQuestion(question)
                this.giveDescendentOrderAndAppend(questionComponent)
                answers.forEach(activeQuestionAnswer => {
                    this.addAnswerContent(questionComponent,activeQuestionAnswer)    
                });
                this.removeButtonsOfAnswersList()
                this.createTag("Completada", "completedQuestion", questionComponent)
                questionComponent.shadowDOM.querySelector('karma-button').remove()
                this.showsQuestionListTitle()
            }))
    }

    showsQuestionListTitle () {
        const karma = document.querySelector('karma-game')
        karma.shadowDOM.querySelector('#questionListTitle').hidden = false
    }
    createQuestion(question) {
        const questionComponent = document.createElement('one-question')
        const questionTitle = questionComponent.shadowDOM.querySelector('h1')
        questionComponent.id = question.id
        questionTitle.innerHTML = question.question.question
        return questionComponent
    }

    writeQuestion() {
        const questionComponent = document.createElement('one-question')
        const questionTitle = questionComponent.shadowDOM.querySelector('h1')

        this.getQuestions()
            .then(questions => {
                questionComponent.id = questions[questions.length - 1].id
                questionTitle.innerHTML = (questions[questions.length - 1].question.question)
            })
        return questionComponent
    }

    loadActiveQuestion() {
        const questionComponent = document.createElement('one-question')
        const questionTitle = questionComponent.shadowDOM.querySelector('h1')

        this.getActiveQuestion()
            .then(activeQuestion => {
                if (activeQuestion.message == "active question not found") {
                    return
                }
                questionComponent.id = activeQuestion.id
                questionTitle.innerHTML = activeQuestion.question.question

                this.createTag("Activa", "activeQuestion", questionComponent)
                this.giveDescendentOrderAndAppend(questionComponent)
                this.addNotValidatedAnswers(questionComponent, activeQuestion)
                this.showFormToSendAnswers()
                
            })
    }

    showFormToSendAnswers() {
        const karma = document.querySelector('karma-game')
        karma.shadowDOM.querySelector('question-form').hidden = true
        karma.shadowDOM.querySelector('answers-form').hidden = false
        this.showsQuestionListTitle()
    }

    addNotValidatedAnswers(questionComponent, activeQuestion) {
        const answerList = questionComponent.shadowDOM.querySelector("answers-list")
        const answersFromServer =  activeQuestion.answers.answers
        answersFromServer.forEach((answer) => {
            answerList.addAnswer(answer.answer,answer.id)
        })
    }

    addAnswersToQuestion(event) {
        const question = this.shadowDOM.querySelector("one-question")
        this.getQuestions()
            .then(questions => {
                const answers = questions[questions.length - 1].answers
                this.addAnswerContent(question, answers[answers.length - 1]) 
            })
    }

    addAnswerContent(oneQuestion, answer) {
        const answersList = oneQuestion.shadowDOM.querySelector('answers-list')
        answersList.addAnswer(answer.answer,answer.id)
    }

    appendQuestion(event) {
        if (!this.shadowDOM.querySelector("one-question")) {
            return this.addQuestion()
        }
        this.addQuestionBefore()
    }

    createTag(text, className, component) {
        const insideOneQuesiton = component.shadowDOM.querySelector(".questionContainer")
        const tag = document.createElement('span')
        tag.innerHTML = text
        tag.setAttribute("class", className)
        insideOneQuesiton.appendChild(tag)
    }

    addQuestion() {
        const oneQuestion = this.writeQuestion()
        this.shadowDOM.appendChild(oneQuestion)
        this.createTag("Activa", "activeQuestion", oneQuestion)
    }

    addQuestionBefore() {
        const oneQuestion = this.writeQuestion()
        this.giveDescendentOrderAndAppend(oneQuestion)
        this.createTag("Activa", "activeQuestion", oneQuestion)
    }

    giveDescendentOrderAndAppend(component) {
        const questions = this.shadowDOM.querySelectorAll("one-question")
        const lastQuestion = questions.item(0)
        this.shadowDOM.insertBefore(component, lastQuestion)
    }

    changeQuestionStateToClose(event) {
        const question = this.shadowDOM.querySelector("one-question")
        const questionID = question.id 
        this.postQuestion("http://localhost:3000/changeStateToClose", { questionID })
            .then(() => {
                this.removeActiveTag(question)
                this.createTag("Completada", "completedQuestion", question)
                this.removeButtonsOfAnswersList("non validated answer")
            })
    }

    removeButtonsOfAnswersList(item) {
        const currentQuestion = this.shadowDOM.querySelector('one-question')
        const oneAnswers = currentQuestion.shadowDOM.querySelector('answers-list').shadowDOM.querySelectorAll('one-answer')
        oneAnswers.forEach(answer => {
            answer.shadowDOM.querySelector('[name="✓"]').remove()
            answer.shadowDOM.querySelector('[name="✗"]').remove()
            if (item == "non validated answer") {
                this.createNotValidatedTagForAnswers(answer)
            }
        })
    }

    createNotValidatedTagForAnswers(answer) {
        const notValidatedAnswerTag = document.createElement('span')
        notValidatedAnswerTag.innerHTML = "No validada"
        const answerContainer = answer.shadowDOM.querySelector('.container')
        notValidatedAnswerTag.style.margin = "auto"
        answerContainer.appendChild(notValidatedAnswerTag)
    }

    removeActiveTag(question) {
        const activeTag = question.shadowDOM.querySelector(".questionContainer").querySelector("span")
        activeTag.remove()
    }

    async getQuestions() {
        try {
            const response = await fetch(this.url + "/questions")
            const allQuestions = await response.json()
            return allQuestions
        } catch (err) {
            console.log('errooooooooooooooooor')
        }
    }

    async getClosedQuestions() {
        try {
            const response = await fetch(this.url + "/closed")
            const allClosedQuestions = await response.json()
            return allClosedQuestions
        } catch (err) {
            console.log('errooooooooooooooooor')
        }
    }

    async getActiveQuestion() {
        try {
            const response = await fetch(this.url + "/active")
            const activeQuestion = await response.json()
            return activeQuestion
        } catch (err) {
            console.log('errooooooooooooooooor')
        }
    }

    postQuestion(url, question) {
        const postFeatures = {
            method: 'POST',
            body: JSON.stringify(question),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        return fetch(url, postFeatures)
    }
}

window.customElements.define("question-list", QuestionList)