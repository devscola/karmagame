const listTemplate = document.createElement('template')
listTemplate.innerHTML = `
    <style>
        karma-list-item{
            display:block;
        }

        ul {
            font-size: 20px;
            list-style: none;
        }

        li {
            border: 1px solid #E29926;
            max-width: fit-content;
            padding: 10px;
            margin-bottom: 12px;
            background-color: #fff;
            box-shadow: 0 0.5em 1em -0.125em rgba(10,10,10,0.1), 0 0px 0 1px rgba(10,10,10,0.02);
            color: #4a4a4a;
            max-width: 100%;
            position: relative;
        }
    </style>
    <ul></ul>
`

export default class ComponentList extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(listTemplate.content.cloneNode(true))
        this.list = this.shadowDOM.querySelector("ul")
        this.idList = 0
    }

    createLiElement(text){
        const liComponent = document.createElement("li")
        liComponent.innerHTML = text
        liComponent.id = this.idList
        return liComponent
    }

    addReversedOrder(text) {
        if (this.idList!=0){
            this.addBefore(text)
        } else {
            this.addElement(text)
        }
    }

    addElement(text){
        const liElement = this.createLiElement(text)
        this.list.appendChild(liElement)
        this.idList++
    }

    addBefore(text){
        const liElement = this.createLiElement(text)
        const lastListElement = this.shadowDOM.getElementById(this.idList-1)
        this.list.insertBefore(liElement, lastListElement)
        this.idList++
    }

    deleteElement(id){
        const elementToRemove = this.shadowDOM.getElementById(id)
        elementToRemove.remove()
    }

    deleteComponent(){
        document.querySelector("component-list").remove()
    }

    cleanList(){
        while (this.list.firstChild) {
            this.list.removeChild(this.list.firstChild)
        }
    }

    modify(id,text){
        const liComponent = this.shadowDOM.getElementById(id)
        liComponent.innerHTML = text
    }

}

window.customElements.define("component-list", ComponentList)
