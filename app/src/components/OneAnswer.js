import oneAnswerTemplate from '../templates/OneAnswerTemplate.html' 
const answersTemplate = document.createElement('template')
answersTemplate.innerHTML = oneAnswerTemplate

export default class OneAnswer extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(answersTemplate.content.cloneNode(true))
        this.idAnswer = 0
    }
    
    connectedCallback() {
        this.shadowDOM.addEventListener('decline', this.declineAnswer.bind(this))
        this.shadowDOM.addEventListener('accept', this.acceptAnswer.bind(this))
    }

    declineAnswer() {
        this.dispatchEvent(
            new CustomEvent('declineAnswer', {
                bubbles: true,
                detail: this.shadowDOM.querySelectorAll('karma-button')[1].id,
                composed: true
            })
        )
    }

    acceptAnswer() {
        this.dispatchEvent(
            new CustomEvent('acceptAnswer', {
                bubbles: true,
                detail: this.shadowDOM.querySelector('karma-button').id,
                composed: true
            })
        )
    }

    addAnswerText(text) {
        const answerText = this.shadowDOM.querySelector('p')
        answerText.innerHTML = text
    }
}

window.customElements.define("one-answer", OneAnswer)
