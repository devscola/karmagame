const karmaGameTemplate = document.createElement('template')
karmaGameTemplate.innerHTML = `
    <question-form></question-form>
    <answers-form></answers-form>
    <h3 id="questionListTitle" part="test">Preguntas realizadas</h3>
    <question-list></question-list>
`
export default class KarmaGame extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(karmaGameTemplate.content.cloneNode(true))
        this.questionForm = this.shadowDOM.querySelector('question-form')
        this.answersForm = this.shadowDOM.querySelector('answers-form')
        this.questionList = this.shadowDOM.querySelector("question-list")
        this.questionListTitle = this.shadowDOM.querySelector("#questionListTitle")
        this.questionListTitle.hidden = true
        this.hideAnswerForm()
        this.focus(this.questionForm)
        this.url = "http://localhost:3000"
    }
    
    connectedCallback() {
        this.questionForm.addEventListener('sendQuestion', this.showAnswersFormAndQuestionTitle.bind(this))
        document.body.addEventListener("closeQuestionButton", this.closeQuestion.bind(this))
    }

    hideAnswerForm() {
        this.answersForm.hidden = true
    }

    showAnswersFormAndQuestionTitle() {
        this.questionForm.hidden = true
        this.answersForm.hidden = false
        this.questionListTitle.hidden = false
        this.focus(this.answersForm)
    }

    closeQuestion() {
        this.hideAnswerForm()
        this.questionForm.hidden = false
        this.focus(this.questionForm)
    }

    focus(element) {
        element.shadowDOM.querySelector('karma-input').shadowDOM.querySelector('input').focus()
    }

    async getQuestion() {
        const response = await fetch(this.url + "/active")
        const question = await response.json()
        return question
    }
}

window.customElements.define("karma-game", KarmaGame)