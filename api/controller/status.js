export default function status(req, res) {
    res.send('Hello World!')
}

/*
function testStatus() {
    const spy = jest.fn()
    const res = {
        send: spy
    }
    status({}, res)
    expect(spy).toHaveBeenCalled()
}
*/