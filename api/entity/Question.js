import AnswerCollection from './AnswerCollection.js'

export default class Question {
    constructor(text, id) {
      this.question = text
      this.answers = new AnswerCollection()
      this.id = id
      this.isActive = true
    }
  
    addAnswer(answerText) {
        this.answers.addAnswer(answerText)
    }    

    changeAnswerState(answerId, status) {
        this.answers.changeAnswerState(answerId, status)
    }

    toJson() {
        return {
            question: this.question,
            answers: this.answers.toJson(),
            id: this.id,
            isActive: this.isActive,
        }
    }
}