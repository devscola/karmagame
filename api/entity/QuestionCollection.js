import Question from './Question.js'

export default class QuestionCollection {
    constructor() {
        this.questions = []
    }

    addNewQuestion(text) {
      const id = "question_" + this.questions.length
      const newQuestion = new Question(text, id)
      this.questions.push(newQuestion)
    }
  
    getActiveQuestion() {
      const activeQuestions = this.questions.filter((question) => question.isActive)
      if (activeQuestions.length === 0) {
          throw new Error('No active questions')
      }

      return activeQuestions[0]
    }
  
    getClosedQuestions() {
      return this.questions.filter((question) => !question.isActive)
    }
  
    changeStateToClose(requestedValue) {
      this.questions.forEach(element => {
        if(element.question.id == requestedValue){
          element.isActive = false
        }
      })
    }

    toJson() {
      return this.questions.map(question => question.toJson())
    }
}