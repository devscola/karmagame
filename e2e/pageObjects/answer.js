const KARMA_PATH = "/"
const KARMA_GAME = 'karma-game'
const QUESTION_LIST = 'question-list'
const ONE_QUESTION = 'one-question'
const ANSWERS_LIST = 'answers-list'
const ONE_ANSWER = 'one-answer'
const QUESTION_FORM = 'question-form'
const ANSWERS_FORM = "answers-form"
const KARMA_BUTTON = 'karma-button'
const KARMA_INPUT = 'karma-input'
const ERROR_MESSAGE = 'error-message'
const COMPONENT_LIST = 'component-list'


export class Answer {

    constructor() {
        cy.visit(KARMA_PATH)
    }

    getErrorMessage() {
        return this.findElement(ANSWERS_FORM)
            .find(ERROR_MESSAGE).shadow()
            .find('h3')
    }

    typeAnswer(answerText) {
        this.findElement(ANSWERS_FORM)
            .find(KARMA_INPUT).shadow()
            .find('input')
            .type(answerText, { force: true })
        return this
    }

    sendAnswer() {
        this.findElement(ANSWERS_FORM)
            .find('#send-answer').shadow()
            .find('button')
            .click()
        return this
    }

    validateAnswer() {
        this.findAnswersList()
            .find(ONE_ANSWER).shadow()
            .find('karma-button').shadow()
            .first()
            .find('button')
            .click({})
        return this
    }


    declineAnswer() {
        this.findAnswersList()
            .find(ONE_ANSWER).shadow()
            .find('karma-button[eventName="decline"]').shadow()
            .find('button')
            .click()
        return this
    }

    getAnswerText(answerId){
        return this.findAnswersList()
            .find(answerId).shadow()
            .find('p')
    }

    getValidatedAnswer() {
        return this.findAnswersList()
            .find(COMPONENT_LIST).shadow()
            .find('li')
    }

    closeQuestion() {
        cy.get(ANSWERS_FORM)
            .find('#close-question')
            .find('button')
            .click()
    }

    findElement(element) {
        return cy.get(KARMA_GAME).shadow()
            .find(element).shadow()
    }

    findAnswersList() {
        return cy.get(KARMA_GAME).shadow()
            .find(QUESTION_LIST).shadow()
            .find(ONE_QUESTION).shadow()
            .find(ANSWERS_LIST).shadow()
    }

}