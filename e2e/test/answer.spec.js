import { Answer } from "../pageObjects/answer.js"
import { Question } from "../pageObjects/question.js"

const questionText = 'Cuculiforme, malvados?'
const answerText = 'First Answer'
const answerText2 = 'Second Answer'

describe('Answers', () => {
    it(`Should be able to send multiple answers`, () => {
        let answer = new Answer()
        let question = new Question()

        question
            .typeQuestion(questionText)
            .sendQuestion()

        answer
            .typeAnswer(answerText)
            .sendAnswer()
        
        answer
            .getAnswerText('[id="0"]')
            .contains(answerText)

        answer
            .typeAnswer(answerText2)
            .sendAnswer()
        
        answer
            .getAnswerText('[id="1"]')
            .contains(answerText2)
    })

    it(`Should be able to validate multiple answers`, () => {
        let answer = new Answer()
        let question = new Question()

        question
            .typeQuestion(questionText)
            .sendQuestion()

        answer
            .typeAnswer(answerText)
            .sendAnswer()
            .validateAnswer()
        
        answer
            .getValidatedAnswer()
            .contains(answerText)

        answer
            .typeAnswer(answerText2)
            .sendAnswer()
            .validateAnswer()
        
        answer
            .getValidatedAnswer()
            .contains(answerText2)
    })

    it(`Should be able to delete multiple answers`, () => {
    })

    it(`Should show an error after sending an empty answer`, () => {
        let answer = new Answer()
        let question = new Question()

        question
            .typeQuestion(questionText)
            .sendQuestion()

        answer
            .sendAnswer()

        answer
            .getErrorMessage()
            .should('exist')
    })

    it(`Should be able to send answers after an error message`, () => {
        let answer = new Answer()
        let question = new Question()

        question
            .typeQuestion(questionText)
            .sendQuestion()

        answer
            .sendAnswer()
            .getErrorMessage()
            .should('exist')

        answer
            .typeAnswer(answerText)
            .sendAnswer()
            .getAnswerText('[id="0"]')
            .contains(answerText)
    })
})