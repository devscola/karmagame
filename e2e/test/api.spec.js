

const newQuestion = { question: 'Jane? Soy la API' }
describe('Api', ()=>{

  it('Our first GET', function() {
    cy.request('http://localhost:3000/')
      .then((response) => {
        expect(response.body).contains('Hello World!')
    })
  })

  it('post is OK', ()=>{
    cy.request('POST', `http://localhost:3000/new`, newQuestion)
        .then((response) => {
          expect(response.status).to.eq(200)
        })
    })

  xit('post question Jane', ()=>{
    cy.request('POST', `http://localhost:3000/new`, newQuestion)
        .then((response) => {
          expect(response.body[0].question).contains('NO POT SER')
        })
    })
})